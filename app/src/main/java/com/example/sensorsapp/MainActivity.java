package com.example.sensorsapp;

import androidx.appcompat.app.AppCompatActivity;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class MainActivity extends AppCompatActivity {
    public static final String SENSOR_DATA = "X = %6.2f, Y = %6.2f, Z = %6.2f";
    SensorEventListener currentSensorListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView myTextView = findViewById(R.id.myTextView);

        // Get the spinner from the xml
        Spinner dropdown = findViewById(R.id.spinner);

        // Create a list of items for the spinner
        List<String> itemsList = new ArrayList<String>();
        SensorManager mgr = (SensorManager) getSystemService(SENSOR_SERVICE);
        List<Sensor> sensors = mgr.getSensorList(Sensor.TYPE_ALL);
        for (Sensor sensor : sensors) {
            itemsList.add(sensor.getName());
        }

        // Create an adapter to describe how the items are displayed
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, itemsList);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Set the spinners adapter to the previously created one
        dropdown.setAdapter(adapter);

        // Add listener to get the selected sensor
        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = dropdown.getItemAtPosition(position).toString();

                Optional<Sensor> optionalSensor = sensors.stream()
                        .filter(it -> it.getName().equals(selected))
                        .findAny();

                SensorEventListener sensorListener = new SensorEventListener() {
                    @Override
                    public void onSensorChanged(SensorEvent event) {
                        // Show sensor values
                        if (event.values.length <= 1) {
                            myTextView.setText(String.valueOf(event.values[0]));
                        } else {
                            String sensorData = String.format(SENSOR_DATA, event.values[0], event.values[1], event.values[2]);
                            myTextView.setText(sensorData);
                        }
                    }

                    @Override
                    public void onAccuracyChanged(Sensor sensor, int accuracy) {}
                };

                optionalSensor.ifPresent(it -> {
                    SensorManager mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
                    // Unregister listener if sensor changes
                    if (currentSensorListener != null) {
                        mSensorManager.unregisterListener(currentSensorListener);
                        myTextView.setText("No sensor data yet...");
                    }

                    // Register new listener
                    Sensor sensor = mSensorManager.getDefaultSensor(it.getType());
                    // Choose sampling periods for sensor
                    mSensorManager.registerListener(sensorListener, sensor, 2000);
                    currentSensorListener = sensorListener;
                });


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }
}