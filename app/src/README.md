# Sensors List App

Simple exercise in Android

# Sensors list

- Goldfish 3-axis Accelerometer
- Goldfish 3-axis Gyroscope
- Goldfish 3-axis Magnetic field sensor
- Goldfish Orientation sensor
- Goldfish Ambient Temperature sensor
- Goldfish Proximity sensor
- Goldfish Light sensor
- Goldfish Pressure sensor
- Goldfish Humidity sensor
- Goldfish 3-axis Magnetic field sensor (uncalibrated)
- Goldfish 3-axis Gyroscope (uncalibrated)
- Game Rotation Vector Sensor
- GeoMag Rotation Vector Sensor
- Gravity Sensor
- Linear Acceleration Sensor
- Rotation Vector Sensor
- Orientation Sensor

# Useful resources

- https://stackoverflow.com/questions/7936345/view-sensor-list-on-android-device
- https://stackoverflow.com/questions/13377361/how-to-create-a-drop-down-list
- https://www.geeksforgeeks.org/how-to-display-the-list-of-sensors-present-in-an-android-device-programmatically/